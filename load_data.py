import ast
import os

def load_data(data_dir, data, size=None) :
    print('Load {} data ...'.format(data))
    sets_file = os.path.join(data_dir, "sets.txt")
    labels_file = os.path.join(data_dir,  "labels.txt")
    sets = []
    labels = []

    with open(sets_file) as f:
        for i, line in enumerate(f):
            liste = [ast.literal_eval(x) for x in line.strip().split(';')]
            sets.append(liste)

    with open(labels_file) as f:
        for i, line in enumerate(f):
            labels.append(int(line))

    assert len(labels) == len(sets)
    print("- done.")
    if size is not None:
        return sets[0:size],labels[0:size]
    else:
        return sets, labels