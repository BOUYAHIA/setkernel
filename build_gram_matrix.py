import argparse
import os
import logging
from math import *
from numpy import *
import numpy as np
from tqdm import tqdm
from load_data import load_data

parser = argparse.ArgumentParser()
parser.add_argument("--size_train", type=int, default=20000)
parser.add_argument("--size_test", type=int, default=5000)
parser.add_argument("--degree", type=int, default=3)
parser.add_argument("--coef0", type=float, default=1.0)
parser.add_argument('--data_dir', default='/home/guests/abdelali.bouyahia/PycharmProjects/set_transformer/'
                                          'supervised_data/dentaire_clause',help="Directory containing the dataset")

def poly(X, Y, coef0, degree):
    K = np.dot(X, Y.T)
    K += coef0
    K **= degree
    return K

def linear(X, Y):
    return np.dot(X, Y.T)

def get_gram_matrix(X1, X2, data, coef0, degree):
    print('Calculate gram matrix for {} data ...'.format(data))
    X1_count = len(X1)
    X2_count = len(X2)
    # Gram matrix
    K = np.zeros((X1_count, X2_count))
    for i in tqdm(range(X1_count)):
        for j in range(X2_count):
            gram = poly(np.array(X1[i]), np.array(X2[j]), coef0, degree)
            #gram = linear(np.array(X1[i]), np.array(X2[j]))
            K[i, j] = np.sum(gram)
    print("- done.")
    return K

def save_gram_matrix(gram_matrix, filename, save_dir):
    # Create directory if it doesn't exist
    print("Saving in {}...".format(os.path.join(save_dir, filename)))
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    with open(os.path.join(save_dir, filename), 'wb') as f:
        np.save(f, gram_matrix)
    print("- done.")

if __name__ == '__main__':

    args = parser.parse_args()
    size_train = args.size_train
    size_test = args.size_test
    coef0 = args.coef0
    degree = args.degree

    X_train, y_train = load_data(os.path.join(args.data_dir, 'train'), 'train', size_train)
    X_test, y_test = load_data(os.path.join(args.data_dir, 'test'), 'test', size_test)

    gram_train = get_gram_matrix(X_train, X_train, 'train', coef0, degree)
    gram_train_file = 'gram_train_' + str(size_train) + '_poly_' + str(degree) + '.npy'
    label_train_file = 'label_train_' + str(size_train) + '_poly_' + str(degree) + '.npy'
    save_gram_matrix(gram_train, gram_train_file, 'gram_matrix')
    save_gram_matrix(np.array(y_train), label_train_file, 'gram_matrix')

    gram_test = get_gram_matrix(X_test, X_train, 'test', coef0, degree)
    gram_test_file = 'gram_test_' + str(size_train) + '_poly_' + str(degree) + '.npy'
    label_test_file = 'label_test_' + str(size_train) + '_poly_' + str(degree) + '.npy'
    save_gram_matrix(gram_test, gram_test_file, 'gram_matrix')
    save_gram_matrix(np.array(y_test), label_test_file, 'gram_matrix')







