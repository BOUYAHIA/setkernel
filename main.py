"""Train the model"""
import argparse
import os
import ast
import logging
from math import *
from sklearn import svm
from numpy import *
import numpy as np
import json
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from tqdm import tqdm
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV

import time

parser = argparse.ArgumentParser()
parser.add_argument("--size_train", type=int, default=20000)
parser.add_argument("--size_test", type=int, default=5000)
parser.add_argument("--degree", type=int, default=3)
parser.add_argument("--coef0", type=float, default=1.0)
parser.add_argument('--data_dir', default='gram_matrix',help="Directory containing gram matrix and labels")

def load_label_gram_matrix(gram_file, label_file, data):
    print('Load {} data and label...'.format(data))

    with open(gram_file, 'rb') as f:
        gram_matrix = np.load(f)

    with open(label_file, 'rb') as f:
        label = np.load(f)
    print("- done.")

    return gram_matrix, label

def get_score(outputs, labels) :
    return {'confusion_matrix': confusion_matrix(labels, outputs), 'accuracy':accuracy_score(labels, outputs),
              'recall': recall_score(labels, outputs), 'precision': precision_score(labels, outputs),
            'f1_score': f1_score(labels, outputs)}

def save_result(save_dir, filename, dictionnary) :
    # Create directory if it doesn't exist
    print("Saving in {} ...".format(os.path.join(save_dir, filename)))
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    with open(os.path.join(save_dir, filename), "w") as f:
        json.dump(dictionnary, f)
    print("- done.")

if __name__ == '__main__':

    args = parser.parse_args()
    size_train = args.size_train
    size_test = args.size_test
    coef0 = args.coef0
    degree = args.degree
    data_dir = args.data_dir

    gram_train_file = 'gram_train_' + str(size_train) + '_poly_' + str(degree) + '.npy'
    label_train_file = 'label_train_' + str(size_train) + '_poly_' + str(degree) + '.npy'
    gram_test_file = 'gram_test_' + str(size_train) + '_poly_' + str(degree) + '.npy'
    label_test_file = 'label_test_' + str(size_train) + '_poly_' + str(degree) + '.npy'

    train_gram_matrix, train_label = load_label_gram_matrix(os.path.join(data_dir, gram_train_file),
                                                            os.path.join(data_dir, label_train_file), 'train')
    test_gram_matrix, test_label = load_label_gram_matrix(os.path.join(data_dir, gram_test_file),
                                                            os.path.join(data_dir, label_test_file), 'test')

    assert train_gram_matrix.shape[0] == train_label.shape[0]
    assert test_gram_matrix.shape[0] == test_label.shape[0]

    train_label = np.expand_dims(train_label, axis=1)

    # Set the parameters by cross-validation
    tuned_parameters = {'C': [1, 10, 100, 1000, 10000], 'class_weight':['balanced', None]}
    scores = ['precision', 'recall', 'f1_score']
    kfold = 4

    chunks_train_gram_matrix = np.hsplit(train_gram_matrix, kfold)
    chunks_train_label = np.vsplit(train_label, kfold) # a verifier avant de lancer le code si c'est le split a utiliser

    best_avg_score = 0.0

    for score in tqdm(scores):
        print("# Tuning hyper-parameters for %s" % score)
        cv_result = {}
        for C in tqdm(tuned_parameters['C']):
            for class_weight in tuned_parameters['class_weight']:
                clf = svm.SVC(kernel='precomputed', cache_size=30000, C=C, class_weight=class_weight)
                chunks_score = []
                for i in range(kfold) :

                    j = int(((kfold - i - 1)*size_train )/kfold)
                    k = int(size_train /kfold)
                    chunk_train_1_1 = train_gram_matrix[0:j, 0:j]
                    chunk_train_2_1 = train_gram_matrix[j + k:size_train, 0:j]
                    chunk_train_1_2 = train_gram_matrix[0:j, j + k:size_train]
                    chunk_train_2_2 = train_gram_matrix[j+k:size_train, j+k:size_train]

                    chunk_val_1 = train_gram_matrix[0:j, j:j+k]
                    chunk_val_2 = train_gram_matrix[j+k:size_train, j:j + k]

                    cont_train_1_1_train_1_2 = np.concatenate((chunk_train_1_1, chunk_train_1_2), axis=1)
                    cont_train_2_1_train_2_2 = np.concatenate((chunk_train_2_1, chunk_train_2_2), axis=1)
                    gram_train = np.concatenate((cont_train_1_1_train_1_2, cont_train_2_1_train_2_2), axis=0)
                    gram_val = np.concatenate((chunk_val_1, chunk_val_2), axis=0)

                    y_train = np.concatenate(tuple(chunks_train_label[0:kfold-i-1] + chunks_train_label[kfold-i:kfold]), axis=0)
                    y_val = chunks_train_label[kfold-i-1]

                    clf.fit(gram_train, np.squeeze(y_train))

                    pred_test = clf.predict(gram_val.T)
                    chunks_score.append(get_score(pred_test, y_val)[score])

                cv_result[str(C)+'_'+str(class_weight)] = {'C': C,
                                                            'class_weight':class_weight,
                                                            'chunks_score':chunks_score,
                                                           'avg_score': sum(chunks_score) / len(chunks_score)
                    }

                if sum(chunks_score) / len(chunks_score) > best_avg_score:
                    best_avg_score = sum(chunks_score) / len(chunks_score)
                    best_params = cv_result[str(C)+'_'+str(class_weight)]

        save_result('result','result_'+score+'.json',cv_result)
        save_result('result', 'best_params_' + score+'.json', best_params)

        print("Best parameters set found for %s" % score)
        print(best_params)
        print("Best score set found for %s" % score)
        print(best_avg_score)






